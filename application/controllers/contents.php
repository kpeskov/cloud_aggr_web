<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	require_once (APPPATH."third_party/Dropbox/autoload.php");
	use \Dropbox as dbx;

	$appInfo = dbx\AppInfo::loadFromJsonFile(APPPATH."third_party/Dropbox/config.json");
	$webAuth = new dbx\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");

class Contents extends CI_Controller {

	public function __construct()
    {
    	parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');

    }
	
	public function index()
	{
		echo('done');
	}
	
}